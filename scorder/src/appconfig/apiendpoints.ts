import {API_URL} from './env';

const endpoints = {
    register: `${API_URL}/cafe/register`,
    login: `${API_URL}/cafe/login`,
    logout: `${API_URL}/cafe/logout`,
    elevate: `${API_URL}/cafe/elevate`,
    newsong: `${API_URL}/cafe/songs/newSong`,
    addsongs: `${API_URL}/cafe/songs/addSongs`,
    currentsong: `${API_URL}/cafe/playlist/currentSong`,
    nextsongs: `${API_URL}/cafe/playlist/nextSongs`,
    drinks: `${API_URL}/cafe/item/add`,
    pricelist: `${API_URL}/cafe/item/pricelist`,
    occupy: `${API_URL}/cafe/tables/occupy`,
    vacant: `${API_URL}/cafe/tables/vacant`,
    quiz: `${API_URL}/cafe/events/quiz/add`,
    events: `${API_URL}/cafe/events/add`,
    my: `${API_URL}/cafe/events/my`,
    eventsResult: {
        getall(id: number):string{
           return `${API_URL}/cafe/events/placements/${id}`
        }
    },
    tables: {
        index:`${API_URL}/cafe/tables/`,
        gettable():string{
            return `${this.index}get`;
        },
        add(numberOfTables: number): string{
            return `${this.index}add?numberOfTables=${numberOfTables}`
        }
    }
}

export default endpoints;
