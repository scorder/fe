const routes = {
    login: '/login',
    register: '/cafe/register',
    home: '/',
    owner: '/owner',
    music: '/music',
    drinks: '/drinks',
    orders: '/orders',
    events: '/events'
}

export default routes;