import React, { FC, useState } from 'react';
import { Props } from '../../containers/drinks';
import './style.css';
import {
  Form,
  Input,
  Button,
  Select,
  Row
} from 'antd';
import { SSL_OP_SINGLE_DH_USE } from 'constants';
import { measure } from '../../store/drinks/types';
/*onInputChange={(inputValue: any, actionMeta: any) => {
                    if(inputValue!==""){
                      console.log(inputValue);
                      ty.push({value:inputValue, label:inputValue});
                    }
                  }}*/


export const Drink: FC<Props> = ({
  pricelist,
  adddrinks,
  isUserOwner,
}) => {
  const [form] = Form.useForm();

  const price = []
  price.push(
    <table>
      <tr>
        <th className="column4">
          <h2>VRSTA</h2>
        </th>
        <th className="column4">
          <h2>NAZIV</h2>
        </th>
        <th className="column4">
          <h2>KOLIČINA</h2>
        </th>
        <th className="column4">
          <h2>MJERA</h2>
        </th>
        <th className="column4">
          <h2>CIJENA</h2>
        </th>
      </tr>
    </table>
  )

  for (var i in pricelist) {
    price.push(
      <table >
        <tr>
          <th className="column4">
            <h3>{pricelist[i].category}</h3>
          </th>
          <th className="column4">
            <h3>{pricelist[i].name}</h3>
          </th>
          <th className="column4">
            <h3>{pricelist[i].servingSize}</h3>
          </th>
          <th className="column4">
            <h3>{pricelist[i].unitOfMeasure}</h3>
          </th>
          <th className="column4">
            <h3>{pricelist[i].price}kn</h3>
          </th>
        </tr>
      </table>
    )
  }
  const meas = []
  for (let drink in measure) {
    if (!Number(drink) && drink !== '0') { meas.push({ text: drink, value: drink }); }
  }



  return <>

    {isUserOwner
      ?
      <Form
        form={form}
        onFinish={(data) => adddrinks({
          name: data.DrinkName,
          category: data.Type,
          servingSize: data.Amount,
          unitOfMeasure: data.Measure,
          price: data.Price
        })}
        name="drinks"
        scrollToFirstError
      >
        <Form.Item
          label="Naziv pića: "
          name="DrinkName"
          rules={[{ required: true, message: 'Drink name is required.' }]}
        >
          <Input />
        </Form.Item>


        <Form.Item
          label="Vrsta pića: "
          name="Type"
          rules={[{ required: true, message: 'Drink type is required.' }]}
        >
          <Input //Selectoptions={ty} 


          //onChange={(newValue: any, actionMeta: any) => {
          //  console.log(newValue);
          //}
          //}
          />
        </Form.Item>


        <Form.Item
          label="Količina: "
          name="Amount"
          rules={[{ required: true, message: 'Amount of drink is required.' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Mjerna jedinica: "
          name="Measure"
          rules={[{ required: true, message: 'Drink measure is required.' }]}
        >
          <Select options={meas} />

        </Form.Item>

        <Form.Item
          label="Cijena: "
          name="Price"
          rules={[{ required: true, message: 'Price of drink is required.' }]}
        >
          <Input />

        </Form.Item>

        <Form.Item >
          <Button type="primary" htmlType="submit">
            OK
                  </Button>
        </Form.Item>
      </Form>
      : <></>}
    {price}
  </>
}