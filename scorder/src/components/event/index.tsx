import React, { FC, useState } from 'react';
import { Props } from '../../containers/event';
import './style.css';
import {
  Form,
  Input,
  Button,
  Select,
  Row,
  Alert
} from 'antd';
import { SSL_OP_SINGLE_DH_USE } from 'constants';
import { measure } from '../../store/drinks/types';
import NumericInput from 'react-numeric-input';
import { addQuiz } from '../../store/quiz/actions';
import Datetime from 'react-datetime';
import TextArea from 'antd/lib/input/TextArea';
import { format } from 'date-fns';
import Switch from "react-switch";
import Moment from 'react-moment';

export const Event: FC<Props> = ({
  questions,
  addquiz,
  addevent,
  isUserOwner
}) => {
  const [kvizEvent, setkvizEvent] = useState(false)
  const [numberOfQuestions, setNumberOfQuestions] = useState(0)
  const [addQuestion, setAddQuestion] = useState(false)
  const [question, setQuestion] = useState(0)
  const [text, setText] = useState("")
  const [A, setA] = useState("")
  const [B, setB] = useState("")
  const [C, setC] = useState("")
  const [D, setD] = useState("")
  const [correct, setCorrect] = useState("")
  const [eventName, setEventName] = useState("")
  const [start, onStart] = useState("");
  const [end, onEnd] = useState("");
  const [eventDescription, setEventDescription] = useState("")
  
  var dateFormat = require("dateformat");

  return <>
    
    <div >
      <Row>
      <h2>Kviz?</h2>
      <Switch onChange={()=>setkvizEvent(showing=>!showing)}  checked={kvizEvent}/>
      </Row>
      <h2>Naziv eventa:</h2>
      <Input onChange={(e) => setEventName(e.target.value)}></Input>
      <Row>
        <div className="column4">
          <h2>Datum i vrijeme početka</h2>
          <Datetime onChange={(e) => onStart(dateFormat(e, "isoDateTime").split("+")[0])} />
        </div>
        <div className="column4">
          <h2>Datum i vrijeme završetka</h2>
          <Datetime onChange={(e) => onEnd(dateFormat(e, "isoDateTime").split("+")[0])} />
        </div>
      </Row>
      <Row>
        <h2>Opis:</h2>
        <textarea className="opis" onChange={(e) => setEventDescription(e.target.value)}></textarea>
      </Row>
      
    </div>
    {kvizEvent
      ?
      <>
      
        {addQuestion
          ?
          <>
            <b>{question + 1}</b>
            <h2>Tekst pitanja</h2>
            <TextArea className = "tekstPitanja" value={text} onChange={(value1) => setText(value1.target.value)} />
            <div>
              <h2>A</h2>
              <Input value={A} onChange={(value1) => setA(value1.target.value)} />
              <h2>B</h2>
              <Input value={B} onChange={(value1) => setB(value1.target.value)} />
              <h2>C</h2>
              <Input value={C} onChange={(value1) => setC(value1.target.value)} />
              <h2>D</h2>
              <Input value={D} onChange={(value1) => setD(value1.target.value)} />
            </div>
            <div>
              
            <div className="radio"><input type="radio" value="A" name="gender" onChange={(e) => setCorrect(A)} />A  </div>
            <div className="radio"><input type="radio" value="B" name="gender" onChange={(e) => setCorrect(B)} />B  </div>
            <div className="radio"><input type="radio" value="C" name="gender" onChange={(e) => setCorrect(C)} />C  </div>
            <div className="radio"><input type="radio" value="D" name="gender" onChange={(e) => setCorrect(D)} />D  </div>
    </div>
            <Button onClick={() => {
              (A === "" || B === "" || C === "" || D === "" || correct === "" || text === "") ?
                alert("Nepovoljan unos!") :
                questions.push({
                  text: text,
                  numberOfQuestion: question + 1,
                  a: A,
                  b: B,
                  c: C,
                  d: D,
                  correct: correct
                });
              setA(""); setB(""); setC(""); setD(""); setText(""); setCorrect(""); setQuestion(questions.length);
              (questions.length === numberOfQuestions) ? addquiz({ event_name: eventName, beginning_date: start, ending_date: end, event_info: eventDescription, questions: questions }) :
                console.log(questions.length)
            }}>ADD</Button>
          </>
          :
          <>
            
            <h2>Broj pitanja:  </h2>
            <NumericInput onChange={(value1) => setNumberOfQuestions(value1 ? value1 : 0)} />
            <Button onClick={() => setAddQuestion(true)}>Ok</Button>
          </>
        }
      </>
      :
      <>
      <Row>
        <div className="column4">
          <Button onClick={() => { addevent({ event_name: eventName, beginning_date: start, ending_date: end, event_info: eventDescription }) }}>Spremi</Button>
        </div>
        
      </Row></>}
  </>
}