import { Button, Modal, Input, Select } from 'antd';
import React, { FC, useState } from 'react';
import Routes from '../../appconfig/routes';
import { Props } from '../../containers/header';
import logoD from '../../images/coffee.png'
import logoT from '../../images/message.png'
import logoM from '../../images/music.png'
import logoA from '../../images/owner.png'
import logoE from '../../images/event.png'
import YouTube from 'react-youtube';
import { Playlist } from '../../store/music/types';
import './style.css';
import { formatDiagnosticsWithColorAndContext } from 'typescript';



export const Header: FC<Props> = ({
    isUserLoggedIn,
    isUserOwner,
    history,
    currentsong,
    logoutUser,
    loginOwner,
    currentSong
}) => {
    console.log(currentsong)
    const [modalVisible, setModalVisible] = useState(false);
    const [pin, setPin] = useState("");
    let a: 0 | 1;
    a = 1;
    const opts = {
        height: '150',
        width: '250',
        playerVars: { autoplay: a },

    };


    return <>
        <div className="head" id="head">
            <Modal
                visible={modalVisible}
                onOk={() => {
                    setModalVisible(false);
                    var pin1 = (document.getElementById("pass") as HTMLInputElement).value.toString()
                    var pin3 = { pin: pin1 };

                    loginOwner(pin3);
                    console.log(isUserOwner)

                }}
                centered
                closable={false}
                onCancel={() => { setModalVisible(false) }}
            >
                <div className="rent-form">
                    <label> Pin vlasnika lokala:  </label>
                    <Input.Password
                        id="pass"
                    ></Input.Password>
                </div>
            </Modal>
            <p
                onClick={() => {
                    history.push(Routes.home)
                }} className="head-name">
                <b>Scan & Order</b></p>
            <div className="icons">
                {
                    isUserLoggedIn
                        ?
                        <>
                            <img className="logo" src={logoA} alt="coffee" width="50" onClick={() => {
                                setModalVisible(true);
                            }} />
                            {isUserOwner?
                            <img className="logo" src={logoE} alt="coffee" width="50" onClick={() => {
                                history.push(Routes.events)
                            }} />:<></>}
                            <img className="logo" src={logoD} alt="coffee" width="50" onClick={() => {
                                history.push(Routes.drinks)
                            }} />
                            <img className="logo" src={logoT} alt="order" width="45" onClick={() => {
                                history.push(Routes.orders)
                            }} />
                            <img className="logo" src={logoM} alt="music" width="45" onClick={() => {
                                history.push(Routes.music)
                            }} />
                        </>
                        :
                        <></>

                }
            </div>

            <div className="head-login">
                {
                    isUserLoggedIn
                        ?
                        <>
                            <Button
                                onClick={() => logoutUser("")}
                            >
                                Logout
                            </Button>
                        </>
                        :
                        <>
                            <Button
                                onClick={
                                    () => history.push(Routes.login)
                                }
                            >
                                Login
                        </Button>
                        </>
                }
            </div>
        </div>
        {currentsong.id !== 0
            ?
            <>
                <YouTube videoId={currentsong.songLink}
                    opts={opts}

                    onStateChange={(data) => data.data === 0 ? currentSong({ currentsong }) : console.log(false)}
                />
            </>
            :
            <></>}
    </>
}

