import React, {FC} from 'react';
import {
    Form,
    Input,
    Button,
  } from 'antd';
import { Props } from '../../containers/login';
import Routes from '../../appconfig/routes';


export const Login: FC<Props> = ({
    loginUser,
    errorMessage,
    history
}) => {
    const [form] = Form.useForm();
    return <>
    <span className="error-message">{errorMessage}</span>
    <Form
            form={form}
            onFinish={(data) => loginUser({
                email: data.email,
                password: data.password,
            })}
            name="register"
            scrollToFirstError
            >
                <Form.Item
                label="Email"
                name="email"
                rules={[{ required: true, message: 'Email is required.' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    name="password"
                    label="Password"
                    rules={[
                    {
                        required: true,
                        message: 'Password is required.',
                    },
                    ]}
                    hasFeedback
                >
                    <Input.Password />
                </Form.Item>
                <Form.Item >
                    <Button type="primary" htmlType="submit">
                        Login
                    </Button>
                </Form.Item>
            </Form>
            <div>
                Niste registrirani?
                <Button
                    type="link"
                    onClick={() => {
                        history.push(Routes.register)
                    }}
                >
                    Register.
                </Button>
            </div>
        </>
}

