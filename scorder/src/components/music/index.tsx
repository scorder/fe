import React, {FC, useState} from 'react';
import {Props} from '../../containers/music';
import './style.css';
import {
    Form,
    Input,
    Button,
    Row,
    Select
  } from 'antd';
//                <iframe width = "200" height = "100" title={allsongs[i].songLink} src = {"https://www.youtube.com/embed/"+allsongs[i].songLink.replace("https://www.youtube.com/watch?v=", "")} className = "songRow"/>


export const Music: FC<Props> = ({
    songsall,
    updateplaylist,
    allsongs,
    isUserOwner,
    addsongs,
    removesongs,
    nextsongs,
    playsong
}) => {
    
    const [form] = Form.useForm();
    const [add, setAdd] = useState();
    const [remove, setRemove] = useState();
    const next = []
    

    for(var f in nextsongs){
        next.push(
            <tbody>
                <h2>{nextsongs[f].songTitle}</h2>
            </tbody>
        )
    }
    

    const songtitleadd = [];
    const songtitleremove = [];
    const artistname = [];
    const albumtitle = [];
    for(var i in allsongs){
        if(!allsongs[i].isInSongList){
            songtitleadd.push({label:allsongs[i].songTitle, value:(allsongs[i].id).toString()});
        }
        else{
            songtitleremove.push({label:allsongs[i].songTitle, value:(allsongs[i].id).toString()});
        }
        artistname.push({label:allsongs[i].artistName, value:allsongs[i].artistName});
        albumtitle.push({label:allsongs[i].albumTitle, value:allsongs[i].albumTitle});
    }

    return <>
            {isUserOwner
            ?
            <>
            <Form 
                form={form}
                onFinish={(data) => songsall({
                    songTitle: data.songTitle,
                    artistName: data.artistName,
                    songLink: data.songLink,
                    albumTitle:data.albumTitle
                })}
                name="songs"                
                scrollToFirstError
                >
                    <Form.Item
                        label="Naziv pjesme: "
                        name="songTitle"
                        id = "songTitle"
                        rules={[{ required: true, message: 'Song name is required.' }]}
                    >
                    <Input /*isClearable options={songtitle}*/ />
                    </Form.Item>

                    <Form.Item
                        label="Naziv izvođača: "
                        name="artistName"
                        id = "artistName"
                     
                    >
                    <Input /*isClearable options={artistname}*/ /> 
                    </Form.Item>
                    <Form.Item
                        label="Naziv albuma: "
                        name="albumTitle"
                        id = "albumTitle"
                        
                    >
                    <Input /*isClearable options={albumtitle}*/ />
                 </Form.Item>
                 <Form.Item
                        label="Link na yt pjemse"
                        name="songLink"
                        rules={[{ required: true, message: 'Song link required only for adding new songs.' }, 
                        ({ getFieldValue }) => ({
                            validator(rule, value) {
                                
                                
                                
                                if (value && value.includes("https://www.youtube.com/watch?v=")) {
                                    
                                    for(var i in allsongs){
                                        
                                        if( allsongs[i].songLink === value){
                                        return Promise.reject("Can't add two same links");
                                        }
                                    }
                                    return Promise.resolve();
                                        
                                    }                    
                           return Promise.reject('It is necessary to follow the link, find the desired address and copy the resulting url');
                    }})
                    ]}
                    >
                    <Input />
                </Form.Item>
                

                <Form.Item >
                    <Button type="primary" htmlType="submit" 
                        >
                        Dodaj
                    </Button>
                </Form.Item>
            </Form>

            <Row className="red">
                    <h2 className="column1"><b>Pjesme za dodati:</b></h2>
                    <Select className="column1" options={songtitleadd} onSelect={(value)=> addsongs.includes(Number(value))?console.log(addsongs):addsongs.push(Number(value))}/>
                    
                    <h2 className="column1"><b>Pjesme za maknuti:</b></h2>
                    <Select className="column1" options={songtitleremove} onSelect={(value)=> removesongs.includes(Number(value))?console.log(removesongs) : removesongs.push(Number(value))}/>
            </Row>

            <div className="allSongs" id="list">
                <Button onClick={()=>updateplaylist({addsongs, removesongs, playsong})}>Spremi promjene</Button>
            </div>
            
            
            </>
            :
            <>
            <Row >
            <h2 className="column"><b>Sve pjesme</b></h2>
                <Select className="column" options={songtitleremove} onSelect={(value)=> playsong = allsongs.filter(s=> s.id === Number(value))[0]}/>
                <Button className="column" onClick= {() => updateplaylist({addsongs, removesongs, playsong})}>Pusti</Button>
                </Row>
                <hr />
                {next}
            </>
            }
         </>
}

