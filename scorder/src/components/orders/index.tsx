import React, { FC, useEffect, useState } from 'react';
import { Props } from '../../containers/orders';
import * as Stomp from "stompjs";
import './style.css';
import {
    Form,
    Input,
    Button,
    InputNumber,
    Row,
    Switch,
    Select
} from 'antd';
/*<div className="column">
        <h2><b>{tables[i].id}</b></h2>
        </div>*/


export const Orders: FC<Props> = ({
    isUserLoggedIn,
    isUserOwner,
    tables,
    addtables,
    cafeId,
    occupy,
    vacate,
    broj,
    ocupy,
    vaca

}) => {
    const [nTables, setNtables] = useState(1)
    const QR = []
    const [broj1, setBroj1] = useState(0)
    const [broj2, setBroj2] = useState([0])
    var QRCode = require('qrcode.react');

    const [getMessage, setMessage] = useState("{}")
    const [order, setOrder] = useState(" ")
    const [name, setName] = useState(["", ""])

    const occ = []
    const vacc = []
    const imena = []

    for (var ime in name) {
        imena.push(
            <Row >
                <h2 className="column1">
                    {name[ime]}
                </h2>
                <h2 className="column1">
                    {broj1}
                </h2>
                <h2 className="column1">
                    {broj2[ime] * broj1}kn
            </h2>
            </Row>

        )
    }

    for (var i in tables) {
        ocupy.push(Number(i) + 1)
        occ.push({ label: (Number(i) + 1), value: (Number(i) + 1) })
        vaca.push(Number(i) + 1)
        vacc.push({ label: (Number(i) + 1), value: (Number(i) + 1) })
        //U buttone za oslobodi i zauzmi trebaju funkcije: occupy i vacate
        QR.push(
            <Row className="red">
                <div className="column">
                    <h2><b>{tables[i].tableNumber}</b></h2>
                </div>

                <div className="column">
                    {JSON.parse(getMessage).tableId === tables[i].id ?
                        imena
                        :
                        ""}
                </div>

                <QRCode value={tables[i].id.toString()} />
            </Row>

        )
    }

    //


    type StompClientState = Stomp.Client;

    const [stompClient, setStompClient] = useState({} as StompClientState);
    const [orderSubscription, setOrderSubscription] = useState(
        {} as Stomp.Subscription
    );
    const sc = Stomp.client("wss://hr-fer-scorder.herokuapp.com/api/ws/websocket");
    useEffect(() => {
        sc.connect(
            {},
            () => {
                const subscription = sc.subscribe(
                    "/api/topic/neworders/" + cafeId,
                    (response) => {
                        //    console.log(JSON.parse(response.body).orderEntries);
                        //    setOrder1(JSON.parse(response.body).orderEntries);
                        setMessage(response.body);
                        var drinkName: string[] = []
                        var drinkCost: number[] = []
                        var jsonInput = JSON.parse(response.body).orderEntries;
                        console.log(JSON.parse(response.body).orderEntries);
                        //odgovor se pretvara u objekt s JSON.parse(response.body)
                        for (var kod in jsonInput) {
                            setBroj1(Number(jsonInput[kod].quantity))
                            drinkName.push(jsonInput[kod].itemResponseDto.name)
                            drinkCost.push(jsonInput[kod].itemResponseDto.price)
                        }
                        setName(drinkName)
                        setBroj2(drinkCost)
                        /*var x = response.body.split("[")[1].split("]")[0]
                         var t = x.split('{')
                         for (var i in t){
                             var i1: number= +i
                             if((i1+1)%2===0 && i1!==0){
                                 var y = x.split('{')[2].split('}')[0]
                                 var y1 = y.split(':')[2].split(',')[0]
                                 console.log(y1)
                                 setOrder(order+y1)
                             }
                             
                         }*/

                    }
                );
                setOrderSubscription(subscription);
                setStompClient(sc);
            }
        );
    }, [cafeId])


    return <>
        <Row className="red">
            <div className="column2">
                <Row>
                    <h2>OSLOBODI: </h2>
                    <Select options={occ} onSelect={(value) => vacate({ id: Number(value) })} />
                </Row>
            </div>
            <div className="column2">
                <Row>
                    <h2>ZAUZMI: </h2>
                    <Select options={vacc} onSelect={(value) => occupy({ id: Number(value) })} />
                </Row>
            </div>
        </Row>
        <div>
            {QR}
        </div>
        {isUserOwner
            ?
            <>
                <p>Broj stolova koje želite dodati:</p>
                <InputNumber
                    min={1}
                    value={nTables}
                    onChange={e => {
                        if (!e) return;
                        if (typeof e === 'string') {
                            setNtables(parseInt(e));
                        } else {
                            setNtables(e);
                        }
                    }}
                />
                <Button onClick={() => addtables(nTables)}>OK</Button>
            </>
            :
            <></>}


    </>

}
