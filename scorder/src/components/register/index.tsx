import React, {
    FC, useState, useRef, useEffect, useCallback
} from 'react';
import { Props } from '../../containers/register';
import {
    Form,
    Input,
    Button,
} from 'antd';
import './style.css';
import GoogleMapReact from 'google-map-react';

/*<div style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyBvllSWXphi2XL2w8OP9A39xrGYysPgrXo', libraries: 'places'}}
          defaultCenter={{
            lat: 59.95,
            lng: 30.33
          }}
          defaultZoom={11}
          
        >
          
        </GoogleMapReact>
      </div>


const googleMapURL = 'https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBvllSWXphi2XL2w8OP9A39xrGYysPgrXo'

function initAutocomplete() {
  const map = new google.maps.Map(
    document.getElementById("map") as HTMLElement,
    {
      center: { lat: -33.8688, lng: 151.2195 },
      zoom: 13,
      mapTypeId: "roadmap",
    }
  );

  // Create the search box and link it to the UI element.
  const input = document.getElementById("pac-input") as HTMLInputElement;
  const searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener("bounds_changed", () => {
    searchBox.setBounds(map.getBounds() as google.maps.LatLngBounds);
  });

  let markers: google.maps.Marker[] = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener("places_changed", () => {
    const places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach((marker) => {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    const bounds = new google.maps.LatLngBounds();
    places.forEach((place) => {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      const icon = {
        url: place.icon as string,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25),
      };

      // Create a marker for each place.
      markers.push(
        new google.maps.Marker({
          map,
          icon,
          title: place.name,
          position: place.geometry.location,
        })
      );

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
}*/




export const Register: FC<Props> = ({
    registerUser,
    errorMessage,
    history,
}) => {
    const [form] = Form.useForm();
  
    return (<>

        <link rel="stylesheet" type="text/css" href="./style.css" />
        <span className="error-message">{errorMessage}</span>

        <Form
            form={form}
            onFinish={(data) => registerUser({
                username: data.username,
                email: data.email,
                password: data.password,
                location: data.location,
                pin: data.pin,
            })}
            name="register"
            scrollToFirstError
        >
            <Form.Item
                label="Naziv lokala"
                name="username"
                rules={[{ required: true, message: 'Username is required.' }]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Email"
                name="email"
                rules={[{ required: true, message: 'Email is required.' }]}
            >
                <Input />
            </Form.Item>

            <a href="https://www.google.hr/maps/@45.7881295,15.9574299,14z" target="_blank">Link for google maps</a>
            <Form.Item
                label="Link lokacije"
                name="location"
                rules={[
                    {
                        required: true,
                        message: 'Location is required.',
                    },
                    ({ getFieldValue }) => ({
                        validator(rule, value) {

                            if (value && value.includes("https://www.google.hr/maps/place/") && value.includes("@")) {
                                var loc = value.split('@');

                                if (loc.size > 0 || loc[1].includes(",")) {
                                    var loc1 = loc[1].split(',');
                                    if (loc1.length > 0) {
                                        if (Number(loc1[0]) && Number(loc1[1])) {
                                            alert("Adress on coordinates:" + loc1[0] + ", " + loc1[1])
                                            return Promise.resolve();
                                        }
                                    }
                                }
                            }
                            return Promise.reject('It is necessary to follow the link, find the desired address and copy the resulting url');
                        },
                    }),
                ]}
            >
                <Input />

            </Form.Item>

            <Form.Item
                name="password"
                label="Password"
                rules={[
                    {
                        required: true,
                        message: 'Password is required.',
                    },
                ]}
                hasFeedback
            >
                <Input.Password />
            </Form.Item>

            <Form.Item
                name="confirm"
                label="Potvrdi Password"
                dependencies={['password']}
                hasFeedback
                rules={[
                    {
                        required: true,
                        message: 'Confirm password is required.',
                    },
                    ({ getFieldValue }) => ({
                        validator(rule, value) {
                            if (!value || getFieldValue('password') === value) {
                                return Promise.resolve();
                            }
                            return Promise.reject('Passwords do not match.');
                        },
                    }),
                ]}
            ><Input.Password />

            </Form.Item>
            <Form.Item
                name="pin"
                label="Pin"
                rules={[
                    {
                        required: true,
                        message: 'Only numbers, max 8 symbols.',
                    },
                    ({ getFieldValue }) => ({
                        validator(rule, value) {
                            if (value.length < 8) {
                                if (Number(value)) {
                                    return Promise.resolve();
                                }
                            }
                            return Promise.reject('Only numbers, max 8 symbols.');
                        },
                    }),
                ]}
                hasFeedback
            >
                <Input.Password />
            </Form.Item>
            <Form.Item >
                <Button type="primary" htmlType="submit">
                    Register
                    </Button>
            </Form.Item>
        </Form>

        
    </>
    )
}