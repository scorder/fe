import React, { FC, useEffect, useState } from 'react';
import logo from '../../images/Scan&Order.jpeg'
import { Props } from '../../containers/userProfile';

import {
    Button,
    Row
} from 'antd';
import './style.css';
import { loadEventResult } from '../../store/user/actions';
import { visitLexicalEnvironment } from 'typescript';

export const User: FC<Props> = ({
    eventi,
    isUserLoggedIn,
    eventResult1,
    eventResult
}) => {
    const upcomingEvents = []
    const [id1, setID] = useState(0)
    const eventPlaces = []
    if (eventResult1.length!==0){
    eventPlaces.push(
        
        <table >
                <tr>

                    <th className="column4">
                        <h4><b>MJESTO</b></h4>
                    </th>
                    <th className="column4">
                        <h4><b>IME</b></h4>
                    </th>
                    <th className="column4">
                        <h4><b>POSTOTAK RJEŠENOSTI</b></h4>
                    </th>
                </tr>
            </table>
    )
    }

    for (var k in eventResult1) {
        eventPlaces.push(

            <table >
                <tr>

                    <th className="column4">
                        <h4>{eventResult1[k].place}.</h4>
                    </th>
                    <th className="column4">
                        <h4>{eventResult1[k].username}</h4>
                    </th>
                    <th className="column4">
                        <h4>{eventResult1[k].percentage*100}%</h4>
                    </th>
                </tr>
            </table>
        )
    }

    for (var i in eventi) {
        const id = eventi[i].id
        const x = Date.parse(eventi[i].date)
        const p = new Date(x)
        const pocetak = p.toUTCString()
        const x2 = Date.parse(eventi[i].ending)
        const p2 = new Date(x2)
        const kraj = p2.toUTCString()
        upcomingEvents.push(
            <tbody>
                <div className="event" onClick={() => {
                    eventResult({ eventId: id });
                    setID(id)
                }}>
                    <h2>Nadolazeći Event</h2>
                    <Row>
                        <h3>Naziv eventa: </h3>
                        <h3>{eventi[i].eventName}</h3>
                    </Row>
                    <Row>
                        <h3>Početak: </h3>
                        <h3>{pocetak.split('GMT')[0]}</h3>
                    </Row>
                    <Row>
                        <h3>Kraj: </h3>
                        <h3>{kraj.split('GMT')[0]}</h3>
                    </Row>
                    <Row>
                        <h3>Opis: </h3>
                        <h3>{eventi[i].eventInfo}</h3>
                    </Row>
                    {id === id1
                        ?
                        <>
                            {eventPlaces}
                        </>
                        :
                        <></>}
                </div>

            </tbody>
        )
    }

    return <div>
        {isUserLoggedIn
            ?
            <>{upcomingEvents}</>
            :
            <></>
        }

        <img src={logo} alt="Mobitel" width="300" />
    </div>
}
//<Button onClick={()=> eventResult({eventId: eventi[0].id})}>K</Button>