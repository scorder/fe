import {Dispatch} from 'react';
import {connect} from 'react-redux';
//import { Drinks } from '../../components/drinks';
import {ApplicationState} from '../../store/types';
import { withRouter } from 'react-router-dom';
import { RouterProps } from 'react-router';
import { logoutUserAction } from '../../store/user/actions';
import { stringify } from 'query-string';
import { Actions, AddDrinkRequestPayload, PriceList, measure  } from '../../store/drinks/types';
import { addDrinks} from '../../store/drinks/actions';
import { Drink } from '../../components/drinks';


interface PropsFromState {
    isUserLoggedIn: boolean;
    pricelist: PriceList[];
    isUserOwner: boolean;
}

interface PropsWithDispatch {
    
    adddrinks: (data: AddDrinkRequestPayload) => void;
    
}

const mapStateToProps = (state: ApplicationState): PropsFromState => {
    console.log(state.order?.tables)
    console.log(state.order?.pricelist)
    return {
        isUserLoggedIn: Boolean(state.user.auth?.accessToken && state.user.auth?.accessToken !== ""),
        pricelist: state.order?.pricelist ?? [],
        isUserOwner: Boolean(state.user.auth?.owner)
    };
};

const mapDispatchToProps = (dispatch: Dispatch<Actions>): PropsWithDispatch => {
    return {
            adddrinks: (data: AddDrinkRequestPayload): void =>{
                dispatch(addDrinks.request(data));
            }
    };
};

export interface Props extends PropsFromState, PropsWithDispatch, RouterProps {}

export default withRouter(connect<PropsFromState, PropsWithDispatch, {}, ApplicationState>(
    mapStateToProps,
    mapDispatchToProps
)(Drink));