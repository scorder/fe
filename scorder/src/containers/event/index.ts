import {Dispatch} from 'react';
import {connect} from 'react-redux';
//import { Drinks } from '../../components/drinks';
import {ApplicationState} from '../../store/types';
import { withRouter } from 'react-router-dom';
import { RouterProps } from 'react-router';
import { Event } from '../../components/event';
import {Actions, Questions, Quiz, Event1} from '../../store/quiz/types'
import { addQuiz, addEvent } from '../../store/quiz/actions';


interface PropsFromState {
    isUserOwner: boolean;
    questions: Questions[];
}

interface PropsWithDispatch {
    
    addquiz: (data: Quiz) => void;
    addevent: (data: Event1) => void;
    
}

const mapStateToProps = (state: ApplicationState): PropsFromState => {
    return {
        questions: [],
        isUserOwner: Boolean(state.user.auth?.owner)
    };
};

const mapDispatchToProps = (dispatch: Dispatch<Actions>): PropsWithDispatch => {
    return {
            addquiz: (data: Quiz): void =>{
                dispatch(addQuiz.request(data));
            },
            addevent: (data: Event1): void =>{
                dispatch(addEvent.request(data));
            }
    };
};

export interface Props extends PropsFromState, PropsWithDispatch, RouterProps {}

export default withRouter(connect<PropsFromState, PropsWithDispatch, {}, ApplicationState>(
    mapStateToProps,
    mapDispatchToProps
)(Event));