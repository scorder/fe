import {Dispatch} from 'react';
import {connect} from 'react-redux';
import { Header } from '../../components/header';
import {ApplicationState} from '../../store/types';
import { Actions } from '../../store/user/types';
import { withRouter } from 'react-router-dom';
import { RouterProps } from 'react-router';
import { logoutUserAction, loginOwnerAction, loadCurrentSong} from '../../store/user/actions';
import { LoginOwnerRequestPayload, LogoutUserRequestPayload, CurrentSongRequestPayload} from '../../store/user/types';
import {Playlist, AllSongsList} from '../../store/music/types';


interface PropsFromState {
    isUserLoggedIn: boolean;    
    isUserOwner: boolean;
    currentsong: AllSongsList;
}

interface PropsWithDispatch {
    logoutUser:(data: LogoutUserRequestPayload) => void;
    loginOwner: (data: LoginOwnerRequestPayload) => void;
    currentSong: (data: CurrentSongRequestPayload) => void;
}

const mapStateToProps = (state: ApplicationState): PropsFromState => {
    const {
        user:{
            errorMessage,
        }
    } = state;
    return {
        isUserLoggedIn: Boolean(state.user.auth?.accessToken),
        isUserOwner: Boolean(state.user.auth?.owner),
        currentsong: state.user.currentsong ?? {songTitle:"", artistName:"", songLink:"", albumTitle:"", id:0, isInSongList:false}
    };
};
const mapDispatchToProps = (dispatch: Dispatch<Actions>): PropsWithDispatch => ({
    logoutUser: (data : LogoutUserRequestPayload): void => dispatch(logoutUserAction.request(data)),
    loginOwner: (data: LoginOwnerRequestPayload): void => dispatch(loginOwnerAction.request(data)),
    currentSong: (data : CurrentSongRequestPayload): void => dispatch(loadCurrentSong.request(data))
});

export interface Props extends PropsFromState, PropsWithDispatch, RouterProps {}

export default withRouter(connect<PropsFromState, PropsWithDispatch, {}, ApplicationState>(
    mapStateToProps,
    mapDispatchToProps
)(Header));