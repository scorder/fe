import {Dispatch} from 'react';
import {connect} from 'react-redux';
import { Music } from '../../components/music';
import {ApplicationState} from '../../store/types';
import { Actions, AddSongsRequestPayload, AllSongsList, AddExistingSongsRequestPayload } from '../../store/music/types';
import { withRouter } from 'react-router-dom';
import { RouterProps } from 'react-router';
import { addSongs, addExistingSongs } from '../../store/music/actions';

interface PropsFromState {
    isUserLoggedIn: boolean;
    isUserOwner: boolean;
    allsongs: AllSongsList[];
    nextsongs: AllSongsList[];
    addsongs: Number[];
    removesongs: Number[];
    playsong: AllSongsList;
}

interface PropsWithDispatch {
    songsall: (data: AddSongsRequestPayload) => void;
    updateplaylist: (data: AddExistingSongsRequestPayload) => void;
    
}

const mapStateToProps = (state: ApplicationState): PropsFromState => {
    return {
        isUserLoggedIn: Boolean(state.user.auth?.accessToken && state.user.auth?.accessToken !== ""),
        isUserOwner: Boolean(state.user.auth?.owner),
        allsongs: state.music?.allsongs ?? [],
        nextsongs: state.music?.nextsongs ?? [],
        addsongs: [],
        removesongs: [],
        playsong: {songTitle:"", artistName:"", songLink:"", albumTitle:"", id:0, isInSongList:false}

    };
};

const mapDispatchToProps = (dispatch: Dispatch<Actions>): PropsWithDispatch => {
    return {
            songsall: (data: AddSongsRequestPayload): void =>{
                dispatch(addSongs.request(data));
            },
            updateplaylist: (data: AddExistingSongsRequestPayload): void => {
                dispatch(addExistingSongs.request(data));
            }
    };
};

export interface Props extends PropsFromState, PropsWithDispatch, RouterProps {}

export default withRouter(connect<PropsFromState, PropsWithDispatch, {}, ApplicationState>(
    mapStateToProps,
    mapDispatchToProps
)(Music));