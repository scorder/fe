import {Dispatch} from 'react';
import {connect} from 'react-redux';
import { Orders } from '../../components/orders';
import {ApplicationState} from '../../store/types';
import { Actions, AddTablesSuccessPayload } from '../../store/orders/types';
import { withRouter } from 'react-router-dom';
import { RouterProps } from 'react-router';
import {Table, table} from '../../store/orders/types'
import { addTables, occupyTable, vacant } from '../../store/orders/actions';


interface PropsFromState {
    isUserLoggedIn: boolean;
    isUserOwner: boolean;
    tables: Table[];
    cafeId: string
    broj: table;
    ocupy: Number[];
    vaca: Number[];
    
}

interface PropsWithDispatch {
    addtables: (data: number) => void;
    occupy: (data: table) => void;
    vacate: (data: table) => void;
    
}

const mapStateToProps = (state: ApplicationState): PropsFromState => {
    return {
        isUserLoggedIn: Boolean(state.user.auth?.accessToken && state.user.auth?.accessToken !== ""),
        isUserOwner: Boolean(state.user.auth?.owner),
        tables: state.order?.tables ?? [],
        cafeId: state.user?.id ?? "",
        broj: {id:1},
        vaca: [0],
        ocupy: [0]
    };
};

const mapDispatchToProps = (dispatch: Dispatch<Actions>): PropsWithDispatch => {
    return {
        addtables: (data: number): void =>{
            dispatch(addTables.request(data));
        },
        occupy: (data: table): void=>{
            dispatch(occupyTable.request(data));
        },
        vacate: (data: table): void=>{
            dispatch(vacant.request(data));
        },
    };
};

export interface Props extends PropsFromState, PropsWithDispatch, RouterProps {}

export default withRouter(connect<PropsFromState, PropsWithDispatch, {}, ApplicationState>(
    mapStateToProps,
    mapDispatchToProps
)(Orders));