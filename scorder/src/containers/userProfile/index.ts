import {connect} from 'react-redux';
import {Dispatch} from 'react';
import { User } from '../../components/userProfile';
import {ApplicationState} from '../../store/types';
import { withRouter } from 'react-router-dom';
import { RouterProps } from 'react-router';
import { event2 } from '../../store/user/types'
import { EventResultRequestPayload } from '../../store/user/types';
import { Actions } from '../../store/user/types';
import { loadEventResult} from '../../store/user/actions';
import {result } from '../../store/user/types'


interface PropsFromState {
    email: String;
    eventi: event2[];
    isUserLoggedIn: boolean;
    eventResult1: result[];
}

interface PropsWithDispatch {
    eventResult: (data: EventResultRequestPayload) => void;
}

const mapStateToProps = (state: ApplicationState): PropsFromState => {
    return {
        email: state.user.email ?? '',
        eventi : state.user.upcommingevents ?? [],
        isUserLoggedIn: Boolean(state.user.auth?.accessToken),
        eventResult1: state.user.eventResult ?? [],
    };
};

const mapDispatchToProps = (dispatch: Dispatch<Actions>): PropsWithDispatch => ({
    eventResult: (data: EventResultRequestPayload): void => dispatch(loadEventResult.request(data)),
});

export interface Props extends PropsFromState, PropsWithDispatch, RouterProps {}

export default withRouter(connect<PropsFromState, PropsWithDispatch, {}, ApplicationState>(
    mapStateToProps,
    mapDispatchToProps
)(User));