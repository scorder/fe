import { 
    AddDrinkRequestPayload,
    AddDrinkSuccessPayload
} from './types';
import { createAsyncAction } from 'typesafe-actions';

import {
    ADD_DRINKS_REQUEST,
    ADD_DRINKS_SUCCESS,
    ADD_DRINKS_FAILURE,

    

} from './constants';

export const addDrinks = createAsyncAction(
    ADD_DRINKS_REQUEST,
    ADD_DRINKS_SUCCESS,
    ADD_DRINKS_FAILURE,

)<AddDrinkRequestPayload, AddDrinkSuccessPayload, Error>();




