
import {createReducer} from 'typesafe-actions';
import { addDrinks } from './actions';
import { DrinkState} from './types';
import {Actions} from './types';

export const initialState: DrinkState = {}

export const drinksReducer = createReducer<DrinkState, Actions>(initialState)
                                //Register actions
                                .handleAction(
                                    addDrinks.request,
                                    (state, action) =>({
                                        ...state,
                                        
                                        name: action.payload.name,
                                        category:action.payload.category,
                                        servingSize:action.payload.servingSize,
                                        unitOfMeasure:action.payload.unitOfMeasure,
                                        price:action.payload.price,

                                        errorMessage: ''
                                    })
                                )
                                .handleAction(
                                    addDrinks.success,
                                    (state) => ({
                                        ...state,
                                        
                                        errorMessage:''
                                    })
                                )
                                .handleAction(
                                    addDrinks.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                );
                                //Login actions
                                
                                
                                