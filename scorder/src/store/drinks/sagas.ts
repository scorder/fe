import {
    all,
    fork, 
    put,
    call, 
    takeLatest,
    select,
} from 'redux-saga/effects';
import ApiCall from '../middleware/api';
import Endpoints from '../../appconfig/apiendpoints';
import {ActionType} from 'typesafe-actions';
import { history } from '../root';
import { addDrinks} from './actions';
import {loadPriceList} from '../orders/actions'
import Routes from '../../appconfig/routes';
import {PriceList} from './types'

function* handleaddDrinksRequest(action: ActionType<typeof addDrinks.request>): Generator {
    try{
        const {
            name,
            category,
            servingSize,
            unitOfMeasure,
            price
        } = action.payload;

        const response = (yield call(ApiCall.post, Endpoints.drinks, {
            data: {
                name:name,
                price:price,
                servingSize:servingSize,
                unitOfMeasure:unitOfMeasure,
                percentDiscount:0.0,
                category:category
            }
        }, false)) as {}

        const response1 = (yield call(ApiCall.get, Endpoints.pricelist, {}, false)) as PriceList[];
        
        yield put(loadPriceList.success({
            pricelist: response1
        }));

        yield call(history.push, Routes.drinks);
        } 
    catch (err) {
        yield put(addDrinks.failure(err));
        }
}




function* watchAddDrinksRequestSaga(): Generator {
    yield takeLatest(addDrinks.request, handleaddDrinksRequest);
}


function* drinkSaga(): Generator {
    yield all([
        fork(watchAddDrinksRequestSaga),
    ]);
}

export default drinkSaga;