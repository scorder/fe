import { 
    addDrinks
} from './actions';
import { ActionType } from 'typesafe-actions';

export enum measure{
    ML,
    DL,
    L,
    KOM
}
export interface PriceList{
    id: Number;
    name: string;
    price: Number;
    servingSize: Number;
    unitOfMeasure: string;
    percentDiscount: Number;
    category: string;
}

export interface AddDrinkRequestPayload {
    name: string,
    category: string,
    servingSize: Number,
    unitOfMeasure: string,
    price: Number
}

export interface AddDrinkSuccessPayload {
}



export interface DrinkState{
    pricelist?: PriceList[];
    errorMessage?: string;
    
}

export interface loadPriceListRequestPayload{
}
export type Actions = ActionType<typeof addDrinks >;