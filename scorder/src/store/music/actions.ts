import { 
    AddSongsRequestPayload,
    AddSongsSuccessPayload,
    NextSongsRequestPayload,
    NextSongsSuccessPayload,
    AddExistingSongsRequestPayload,
    AddExistingSongsSuccessPayload

} from './types';
import { createAsyncAction } from 'typesafe-actions';

import {
    ADD_SONGS_REQUEST,
    ADD_SONGS_SUCCESS,
    ADD_SONGS_FAILURE,

    ADD_EXISTING_SONGS_REQUEST,
    ADD_EXISTING_SONGS_SUCCESS,
    ADD_EXISTING_SONGS_FAILURE,

    NEXT_SONGS_REQUEST,
    NEXT_SONGS_SUCCESS,
    NEXT_SONGS_FAILURE,

} from './constants';

export const addSongs = createAsyncAction(
    ADD_SONGS_REQUEST,
    ADD_SONGS_SUCCESS,
    ADD_SONGS_FAILURE,
)<AddSongsRequestPayload, AddSongsSuccessPayload, Error>();

export const addExistingSongs = createAsyncAction(
    ADD_EXISTING_SONGS_REQUEST,
    ADD_EXISTING_SONGS_SUCCESS,
    ADD_EXISTING_SONGS_FAILURE,
)<AddExistingSongsRequestPayload, AddExistingSongsSuccessPayload, Error>();

export const loadNexSongs = createAsyncAction(
    NEXT_SONGS_REQUEST,
    NEXT_SONGS_SUCCESS,
    NEXT_SONGS_FAILURE,
)<NextSongsRequestPayload, NextSongsSuccessPayload, Error>();
