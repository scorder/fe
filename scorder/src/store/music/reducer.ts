
import {action, createReducer} from 'typesafe-actions';
import { addSongs,  loadNexSongs } from './actions';
import {MusicState} from './types'
import {Actions} from './types';

export const initialState: MusicState = {}

export const musicReducer = createReducer<MusicState, Actions>(initialState)
                                //Register actions
                                .handleAction(
                                    addSongs.request,
                                    (state, action) =>({
                                        ...state,
                                        errorMessage: ''
                                    })
                                )
                                .handleAction(
                                    addSongs.success,
                                    (state, action) => ({
                                        ...state,
                                        allsongs: action.payload.allsongs,
                                        errorMessage:''
                                    })
                                )
                                .handleAction(
                                    addSongs.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                )
                                //Login actions
                                
                                
                                .handleAction(
                                    loadNexSongs.request,
                                    (state, action) => ({
                                        ...state,
                                        
                                        errorMessage: ''
                                    })
                                )
                                .handleAction(
                                    loadNexSongs.success,
                                    (state, action) => ({
                                        ...state,
                                        nextsongs: action.payload.nextsongs,
                                        errorMessage: ''
                                    })
                                )
                                .handleAction(
                                    loadNexSongs.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                );