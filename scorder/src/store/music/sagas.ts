import {
    all,
    fork, 
    put,
    call, 
    takeLatest,
    select,
} from 'redux-saga/effects';
import ApiCall from '../middleware/api';
import Endpoints from '../../appconfig/apiendpoints';
import {ActionType} from 'typesafe-actions';
import { history } from '../root';
import { addSongs, loadNexSongs, addExistingSongs} from './actions';
import { loadCurrentSong} from '../user/actions';
import Routes from '../../appconfig/routes';
import { Playlist, AllSongsList } from '../music/types';

function* handleaddSongsRequest(action: ActionType<typeof addSongs.request>): Generator {
    try{
        var {
            songTitle,
            artistName,
            albumTitle,
            songLink,
        } = action.payload;

        
        artistName = artistName ?? ""
        albumTitle = albumTitle ?? ""
        
        if(songLink.startsWith("https://www.youtube.com/watch?v=")){
            const response = (yield call(ApiCall.post, Endpoints.newsong, {
                data: {
                    songTitle: songTitle,
                    artistName: artistName,
                    albumTitle: albumTitle,
                    songLink: songLink.replace("https://www.youtube.com/watch?v=", "")
                }
            }, false)) as {
            }
            const response1 = (yield call(ApiCall.get, Endpoints.addsongs, {
            }, false)) as AllSongsList[];
            const song = response1.filter(song => song.songLink === songLink.replace("https://www.youtube.com/watch?v=", ""));
            const response2 = (yield call(ApiCall.post, Endpoints.addsongs,{
                    data:
                    {
                        songsToAdd:[song[0].id],
                        songsToRemove:[]
                    }
                },false))
            const response3 = (yield call(ApiCall.get, Endpoints.addsongs, {
                }, false)) as AllSongsList[];
            yield put(addSongs.success({
                    allsongs: response3
            }));           
            
        }
        
        

        yield call(history.push, Routes.music);
        } 
    catch (err) {
        yield put(addSongs.failure(err));
        }
}

function* handleaddExistingSongsRequest(action: ActionType<typeof addExistingSongs.request>): Generator {
    try{
        const {
            addsongs,
            removesongs,
            playsong
        } = action.payload;
        
        const response = (yield call(ApiCall.post, Endpoints.addsongs,{
                data:
                {
                    songsToAdd:addsongs,
                    songsToRemove:removesongs,
                }
            },false))
        
        const response1 = (yield call(ApiCall.get, Endpoints.addsongs, {
            }, false)) as AllSongsList[];
        yield put(addSongs.success({
                allsongs: response1
        }));
        
        yield put(loadCurrentSong.success({
            currentsong: playsong
        }));

        yield call(history.push, Routes.music);
        } 
    catch (err) {
        yield put(addSongs.failure(err));
        }
}


function* handleloadNexSongsRequest(action: ActionType<typeof loadNexSongs.request>): Generator {
    const response1 = (yield call(ApiCall.get, Endpoints.nextsongs, {
        }, false)) as AllSongsList[];
    yield put(loadNexSongs.success({
            nextsongs: response1
    }));
   
}

function* watchAddSongRequestSaga(): Generator {
    yield takeLatest(addSongs.request, handleaddSongsRequest);
}

function* watchNexSongsRequestSaga(): Generator {
    yield takeLatest(loadNexSongs.request, handleloadNexSongsRequest)
}

function* watchExistingAddSongRequestSaga(): Generator {
    yield takeLatest(addExistingSongs.request, handleaddExistingSongsRequest)
}

function* musicSaga(): Generator {
    yield all([
        fork(watchAddSongRequestSaga),
        fork(watchNexSongsRequestSaga),
        fork(watchExistingAddSongRequestSaga)
    ]);
}

export default musicSaga;