import { 
    addExistingSongs,
    addSongs,
    loadNexSongs
} from './actions';
import { ActionType } from 'typesafe-actions';



export type AllSongsList = {
    songTitle: string;
    artistName: string;
    albumTitle: string;
    songLink:string;
    isInSongList: boolean;
    id: Number;
}

export interface Playlist{
    songTitle: string;
    artistName: string;
    albumTitle: string;
    songLink:string;
}

export interface AddSongsRequestPayload {
    songTitle: string;
    artistName: string;
    albumTitle: string;
    songLink:string;
}

export interface AddSongsSuccessPayload {
    allsongs: AllSongsList[]
}

export interface NextSongsSuccessPayload {
    nextsongs: AllSongsList[]
}

export interface NextSongsRequestPayload{
    nextsongs: AllSongsList[]
}

export interface AddExistingSongsRequestPayload{
    addsongs: Number[],
    removesongs: Number[],
    playsong : AllSongsList
    
}

export interface AddExistingSongsSuccessPayload{

}

export interface MusicState{
    errorMessage?: string;
    allsongs?: AllSongsList[];
    nextsongs?: AllSongsList[];
}

export type Actions = ActionType<typeof addSongs 
                                | typeof loadNexSongs
                                | typeof addExistingSongs >;