import { 
    AddTablesSuccessPayload,
    loadTablesSuccesPayload,
    loadPriceListSuccessPayload,
    table

} from './types';
import { createAsyncAction } from 'typesafe-actions';

import {
    ADD_TABLES_REQUEST,
    ADD_TABLES_SUCCESS,
    ADD_TABLES_FAILURE,

    TABLES_REQUEST,
    TABLES_SUCCESS,
    TABLES_FAILURE,

    PRICE_LIST_REQUEST,
    PRICE_LIST_SUCCESS,
    PRICE_LIST_FAILURE,

    OCCUPY_REQUEST,
    OCCUPY_SUCCESS,
    OCCUPY_FAILURE,

    VACANT_REQUEST,
    VACANT_SUCCESS,
    VACANT_FAILURE

} from './constants';

export const addTables = createAsyncAction(
    ADD_TABLES_REQUEST,
    ADD_TABLES_SUCCESS,
    ADD_TABLES_FAILURE,

)<number, AddTablesSuccessPayload, Error>();


export const loadTables = createAsyncAction(
    TABLES_REQUEST,
    TABLES_SUCCESS,
    TABLES_FAILURE
)<{}, loadTablesSuccesPayload, Error>();

export const loadPriceList = createAsyncAction(
    PRICE_LIST_REQUEST,
    PRICE_LIST_SUCCESS,
    PRICE_LIST_FAILURE
)<{}, loadPriceListSuccessPayload, Error>();

export const occupyTable = createAsyncAction(
    OCCUPY_REQUEST,
    OCCUPY_SUCCESS,
    OCCUPY_FAILURE,
)<table, {}, Error>();

export const vacant = createAsyncAction(
    VACANT_REQUEST,
    VACANT_SUCCESS,
    VACANT_FAILURE
)<table, {}, Error>();
