export const ADD_TABLES_REQUEST = '@@user/ADD_TABLES_REQUEST';
export const ADD_TABLES_SUCCESS = '@@user/ADD_TABLES_SUCCESS';
export const ADD_TABLES_FAILURE = '@@user/ADD_TABLES_FAILURE';

export const TABLES_REQUEST = `@@user/TABLES_REQUEST`;
export const TABLES_SUCCESS = `@@user/TABLES_SUCCESS`;
export const TABLES_FAILURE = `@@user/TABLES_FAILURE`;

export const PRICE_LIST_REQUEST = `@@user/PRICE_LIST_REQUEST`;
export const PRICE_LIST_SUCCESS = `@@user/PRICE_LIST_SUCCESS`;
export const PRICE_LIST_FAILURE = `@@user/PRICE_LIST_FAILURE`;

export const OCCUPY_REQUEST = `@@user/OCCUPY_REQUEST`;
export const OCCUPY_SUCCESS = `@@user/OCCUPY_SUCCESS`;
export const OCCUPY_FAILURE = `@@user/OCCUPY_FAILURE`;

export const VACANT_REQUEST = `@@user/VACANT_REQUEST`;
export const VACANT_SUCCESS = `@@user/VACANT_SUCCESS`;
export const VACANT_FAILURE = `@@user/VACANT_FAILURE`;



