
import { act } from 'react-dom/test-utils';
import {createReducer} from 'typesafe-actions';
import { addTables,  loadTables, loadPriceList, occupyTable, vacant } from './actions';
import { OrderState} from './types';
import {Actions} from './types';

export const initialState: OrderState = {}

export const orderReducer = createReducer<OrderState, Actions>(initialState)
                                //Register actions
                                .handleAction(
                                    addTables.request,
                                    (state, action) =>({
                                        ...state,
                                        tableNumber: action.payload,
                                        errorMessage: ''
                                    })
                                )
                                .handleAction(
                                    addTables.success,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage:''
                                    })
                                )
                                .handleAction(
                                    addTables.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                )
                                //Login actions
                                
                                
                                .handleAction(
                                    loadTables.request,
                                    (state) => ({
                                        ...state,
                                        errorMessage: ''
                                    })
                                )
                                .handleAction(
                                    loadTables.success,
                                    (state, action) => ({
                                        ...state,
                                        tables: action.payload.tables,
                                        
                                    })
                                )
                                .handleAction(
                                    loadTables.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                )
                                .handleAction(
                                    loadPriceList.request,
                                    (state) => ({
                                        ...state,
                                        errorMessage: '',
                                        
                                    })
                                )
                                .handleAction(
                                    loadPriceList.success,
                                    (state, action) => ({
                                        ...state,
                                        pricelist: action.payload.pricelist,
                                    })
                                )
                                .handleAction(
                                    loadPriceList.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                )
                                .handleAction(
                                    occupyTable.request,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: '',
                                        
                                        
                                    })
                                )
                                .handleAction(
                                    occupyTable.success,
                                    (state,) => ({
                                        ...state,
                                        errorMessage: '',
                                    })
                                )
                                .handleAction(
                                    occupyTable.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                )
                                .handleAction(
                                    vacant.request,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: '',
                                        
                                    })
                                )
                                .handleAction(
                                    vacant.success,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: '',
                                    })
                                )
                                .handleAction(
                                    vacant.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                );
