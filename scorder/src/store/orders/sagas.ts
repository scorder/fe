import {
    all,
    fork, 
    put,
    call, 
    takeLatest,
    select,
} from 'redux-saga/effects';
import ApiCall from '../middleware/api';
import Endpoints from '../../appconfig/apiendpoints';
import {ActionType} from 'typesafe-actions';
import { history } from '../root';
import { addTables, loadTables, occupyTable, vacant} from './actions';
import Routes from '../../appconfig/routes';
import  {Table} from './types';

function* handleaddTablessRequest(action: ActionType<typeof addTables.request>): Generator {
    try{
        const response = (yield call(ApiCall.post, Endpoints.tables.add(action.payload), {}, false)) as Table[]

        const response1 = (yield call(ApiCall.get, Endpoints.tables.gettable(), {}, false)) as Table[]

        yield put(loadTables.success({tables:response1}))
        console.log(response1)

        yield call(history.push, Routes.orders);
        } 
    catch (err) {
        yield put(addTables.failure(err));
        }
}


function* handleloadTablesRequest(action: ActionType<typeof loadTables.request>): Generator {
    try{
        
        const response = (yield call(ApiCall.get, Endpoints.tables.gettable(), {}, false)) as Table[]

        yield put(loadTables.success({tables:response}))

        yield call(history.push, Routes.orders);
        } 
    catch (err) {
        yield put(addTables.failure(err));
        }
}

function* handleloadOccupyRequest(action: ActionType<typeof occupyTable.request>): Generator {
    try{
        const {
            id
        } = action.payload;
        
        const response = (yield call(ApiCall.post, Endpoints.occupy, {
            data:
            {
            tableNumber:id
        }}, false))
        console.log(response)
        } 
    catch (err) {
        yield put(occupyTable.failure(err));
        }
   
}

function* handleloadVacantRequest(action: ActionType<typeof vacant.request>): Generator {
    try{
        const {
            id
        } = action.payload;
        
        const response = (yield call(ApiCall.post, Endpoints.vacant, {
            data:
            {
            tableNumber:id
        }}, false))
        console.log(response)
        } 
    catch (err) {
        yield put(occupyTable.failure(err));
        }
   
}

function* watchAddTablesRequestSaga(): Generator {
    yield takeLatest(addTables.request, handleaddTablessRequest);
}

function* watchLoadTablesRequestSaga(): Generator {
    yield takeLatest(loadTables.request, handleloadTablesRequest)
}

function* watchoccupyTablesRequestSaga(): Generator {
    yield takeLatest(occupyTable.request, handleloadOccupyRequest)
}

function* watchvacantTablesRequestSaga(): Generator {
    yield takeLatest(vacant.request, handleloadVacantRequest)
}


function* OrderSaga(): Generator {
    yield all([
        fork(watchAddTablesRequestSaga),
        fork(watchLoadTablesRequestSaga),
        fork(watchoccupyTablesRequestSaga),
        fork(watchvacantTablesRequestSaga),
    ]);
}

export default OrderSaga;