import { 
    addTables,
    loadTables,
    loadPriceList,
    occupyTable,
    vacant
} from './actions';
import { ActionType } from 'typesafe-actions';
import {PriceList} from '../drinks/types'

export type Table = {
    id: Number;
    tableNumber: Number;
}

export type AddTablesSuccessPayload = {
    tables: Table[];
}

export type loadTablesSuccesPayload = {
    tables: Table[];
}

export type orderEntries = {
    id: Number;
    name: string;
    price: Number;
    servingSize: Number;
    unitOfMeasure: string;
    percentDiscount: Number;
    category: string;
}

export type orderType = {
    id: Number;
    tableId: Number;
    orderEntries: orderEntries[];
    quantity: Number;
    timestamp: Date;
}

export interface loadPriceListSuccessPayload {
    pricelist: PriceList[];
}

export interface table {
    id: Number;
}

export interface OrderState{
    errorMessage?: string;
    tables?: Table[];
    pricelist?: PriceList[];
}


export type Actions = ActionType<typeof  addTables
                                | typeof loadTables
                                | typeof loadPriceList
                                | typeof occupyTable
                                | typeof vacant >;