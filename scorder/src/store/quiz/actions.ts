import { 
    Quiz,
    Event1
} from './types';
import { createAsyncAction } from 'typesafe-actions';

import {
    ADD_QUIZ_REQUEST,
    ADD_QUIZ_SUCCESS,
    ADD_QUIZ_FAILURE,

    ADD_EVENT_REQUEST,
    ADD_EVENT_SUCCESS,
    ADD_EVENT_FAILURE    

} from './constants';

export const addQuiz = createAsyncAction(
    ADD_QUIZ_REQUEST,
    ADD_QUIZ_SUCCESS,
    ADD_QUIZ_FAILURE,

)<Quiz, {}, Error>();

export const addEvent = createAsyncAction(
    ADD_EVENT_REQUEST,
    ADD_EVENT_SUCCESS,
    ADD_EVENT_FAILURE

)<Event1, {}, Error>();




