export const ADD_QUIZ_REQUEST = '@@drinks/ADD_QUIZ_REQUEST';
export const ADD_QUIZ_SUCCESS = '@@drinks/ADD_QUIZ_SUCCESS';
export const ADD_QUIZ_FAILURE = '@@drinks/ADD_QUIZ_FAILURE';

export const ADD_EVENT_REQUEST = '@@drinks/ADD_EVENT_REQUEST';
export const ADD_EVENT_SUCCESS = '@@drinks/ADD_EVENT_SUCCESS';
export const ADD_EVENT_FAILURE = '@@drinks/ADD_EVENT_FAILURE';




