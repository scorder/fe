
import {createReducer} from 'typesafe-actions';
import { addQuiz, addEvent } from './actions';
import { EventState} from './types';
import {Actions} from './types';

export const initialState: EventState = {}

export const eventReducer = createReducer<EventState, Actions>(initialState)
                                //Register actions
                                .handleAction(
                                    addQuiz.request,
                                    (state, action) =>({
                                        ...state,
                                        questions: action.payload.questions,
                                        event_name: action.payload.event_name,
                                        beginning_date:action.payload.beginning_date,
                                        ending_date: action.payload.ending_date,
                                        event_info: action.payload.event_info,
                                        

                                        errorMessage: ''
                                    })
                                )
                                .handleAction(
                                    addQuiz.success,
                                    (state) => ({
                                        ...state,
                                        
                                        errorMessage:''
                                    })
                                )
                                .handleAction(
                                    addQuiz.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                )
                                .handleAction(
                                    addEvent.request,
                                    (state, action) =>({
                                        ...state,
                                        
                                        event_name: action.payload.event_name,
                                        beginning_date:action.payload.beginning_date,
                                        ending_date: action.payload.ending_date,
                                        event_info: action.payload.event_info,
                                        errorMessage: ''
                                    })
                                )
                                .handleAction(
                                    addEvent.success,
                                    (state) => ({
                                        ...state,
                                        
                                        errorMessage:''
                                    })
                                )
                                .handleAction(
                                    addEvent.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                );
                                //Login actions
                                
                                
                                