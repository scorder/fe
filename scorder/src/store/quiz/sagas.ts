import {
    all,
    fork, 
    put,
    call, 
    takeLatest,
    select,
} from 'redux-saga/effects';
import ApiCall from '../middleware/api';
import Endpoints from '../../appconfig/apiendpoints';
import {ActionType} from 'typesafe-actions';
import { history } from '../root';
import { addQuiz, addEvent} from './actions';
import Routes from '../../appconfig/routes';
import {loadUpcommingEvents} from '../../store/user/actions'
import {event2} from '../../store/user/types'

function* handleaddQuizRequest(action: ActionType<typeof addQuiz.request>): Generator {
    try{
        const {
            event_name,
            beginning_date,
            ending_date,
            event_info,
            questions
        } = action.payload;

        const response = (yield call(ApiCall.post, Endpoints.quiz, {
            data: {
                eventName:event_name,
                date:beginning_date,
                ending:ending_date,
                eventInfo:event_info,
                questions: questions

            }
        }, false)) as {}

        const response4 = (yield call(ApiCall.get, Endpoints.my, {
        }, false)) as event2[]

        yield put(loadUpcommingEvents.success({
            upcommingevents:response4
        }));

        yield call(history.push, Routes.home);
        } 
    catch (err) {
        yield put(addQuiz.failure(err));
        }
}

function* handleaddEventequest(action: ActionType<typeof addEvent.request>): Generator {
    try{
        const {
            event_name,
            beginning_date,
            ending_date,
            event_info
        } = action.payload;

        const response = (yield call(ApiCall.post, Endpoints.events, {
            data: {
                
                eventName:event_name,
                date:beginning_date,
                ending:ending_date,
                eventInfo:event_info

            }
        }, false)) as {}

        const response4 = (yield call(ApiCall.get, Endpoints.my, {
        }, false)) as event2[]

        yield put(loadUpcommingEvents.success({
            upcommingevents:response4
        }));

        yield call(history.push, Routes.home);
        } 
    catch (err) {
        yield put(addQuiz.failure(err));
        }
}




function* watchAddQuizRequestSaga(): Generator {
    yield takeLatest(addQuiz.request, handleaddQuizRequest);
}

function* watchAddEventRequestSaga(): Generator {
    yield takeLatest(addEvent.request, handleaddEventequest);
}


function* eventSaga(): Generator {
    yield all([
        fork(watchAddQuizRequestSaga),
        fork(watchAddEventRequestSaga),
    ]);
}

export default eventSaga;