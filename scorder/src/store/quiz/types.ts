
import { ActionType } from 'typesafe-actions';
import { 
    addQuiz,
    addEvent
} from './actions';

export interface Questions {
    text:string,
    numberOfQuestion:number,
    a: string,
    b: string,
    c: string,
    d: string,
    correct: string
}

export interface Quiz{
    event_name: string,
    beginning_date:string,
    ending_date: string,
    event_info: string
    questions: Questions[]
}

export interface Event1{
    event_name: string,
    beginning_date:string,
    ending_date: string,
    event_info: string
}

export interface EventState{
    errorMessage?: string;
}

export type Actions = ActionType<typeof  addQuiz
                                | typeof addEvent>;