import { combineReducers} from 'redux';
import { connectRouter } from 'connected-react-router';
import { all, fork } from 'redux-saga/effects';
import {userReducer} from './user/reducer';
import userSaga from './user/sagas';
import musicSaga from './music/sagas';
import drinkSaga from './drinks/sagas';
import eventSaga from './quiz/sagas';
import { createBrowserHistory } from 'history';
import { musicReducer } from './music/reducer';
import OrderSaga from './orders/sagas';
import {eventReducer} from './quiz/reducer'
import {orderReducer} from './orders/reducer'
import { drinksReducer } from './drinks/reducer';


export const history = createBrowserHistory();

export const rootReducer = combineReducers({
    router: connectRouter(history),
    user: userReducer,
    music: musicReducer,
    drink: drinksReducer,
    order: orderReducer,
    event: eventReducer
});

export function* rootSaga(): Generator {
    yield all([
        fork(userSaga),
        fork(musicSaga),
        fork(drinkSaga),
        fork(OrderSaga),
        fork(eventSaga)
    ]);
}