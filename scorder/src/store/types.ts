import { RouterState } from 'connected-react-router';
import { MusicState } from './music/types';
import { DrinkState } from './drinks/types';
import { UserState } from './user/types';
import {OrderState} from './orders/types'
import {EventState} from './quiz/types'


export interface ApplicationState {
    user: UserState;
    router: RouterState;
    music: MusicState;
    drinks: DrinkState;
    order: OrderState;
    event: EventState;
};