import { 
    RegisterUserRequestPayload,
    LoginUserRequestPayload,
    LoginUserSuccessPayload,
    LoginOwnerRequestPayload,
    LoginOwnerSuccessPayload,
    LogoutUserSuccessPayload,
    LogoutUserRequestPayload,
    CurrentSongRequestPayload,
    CurrentSongSuccessPayload,
    event2,
    UpcommingEventsPayload,
    EventResultSuccessPayload,
    EventResultRequestPayload
} from './types';
import { createAsyncAction } from 'typesafe-actions';

import {
    REGISTER_USER_REQUEST,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_FAILURE,
    LOGIN_USER_REQUEST,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGOUT_USER_REQUEST,
    LOGOUT_USER_SUCCESS,
    LOGOUT_USER_FAILURE,
    LOGIN_OWNER_REQUEST,
    LOGIN_OWNER_SUCCESS,
    LOGIN_OWNER_FAILURE,
        
    CURRENT_SONG_REQUEST,
    CURRENT_SONG_SUCCESS,
    CURRENT_SONG_FAILURE,

    UPCOMMING_EVENTS_REQUEST,
    UPCOMMING_EVENTS_SUCCESS,
    UPCOMMING_EVENTS_FAILURE,

    EVENT_RESULT_FAILURE,
    EVENT_RESULT_REQUEST,
    EVENT_RESULT_SUCCESS

} from './constants';

export const registerUserAction = createAsyncAction(
    REGISTER_USER_REQUEST,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_FAILURE,
)<RegisterUserRequestPayload, void, Error>();

export const loginUserAction = createAsyncAction(
    LOGIN_USER_REQUEST,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE,
)<LoginUserRequestPayload, LoginUserSuccessPayload, Error>();

export const loginOwnerAction = createAsyncAction(
    LOGIN_OWNER_REQUEST,
    LOGIN_OWNER_SUCCESS,
    LOGIN_OWNER_FAILURE,
)<LoginOwnerRequestPayload, LoginOwnerSuccessPayload, Error>();


export const logoutUserAction = createAsyncAction(
    LOGOUT_USER_REQUEST,
    LOGOUT_USER_SUCCESS,
    LOGOUT_USER_FAILURE
)<LogoutUserRequestPayload, LogoutUserSuccessPayload, Error>();

export const loadCurrentSong = createAsyncAction(
    CURRENT_SONG_REQUEST,
    CURRENT_SONG_SUCCESS,
    CURRENT_SONG_FAILURE,
)<CurrentSongRequestPayload, CurrentSongSuccessPayload, Error>();

export const loadUpcommingEvents = createAsyncAction(
    UPCOMMING_EVENTS_REQUEST,
    UPCOMMING_EVENTS_SUCCESS,
    UPCOMMING_EVENTS_FAILURE
)<{}, UpcommingEventsPayload, Error>();

export const loadEventResult = createAsyncAction(
    EVENT_RESULT_REQUEST,
    EVENT_RESULT_SUCCESS,
    EVENT_RESULT_FAILURE
)<EventResultRequestPayload, EventResultSuccessPayload, Error>();




