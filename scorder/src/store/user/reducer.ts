
import {action, createReducer} from 'typesafe-actions';
import { loginUserAction, logoutUserAction, registerUserAction, loginOwnerAction, loadCurrentSong, loadUpcommingEvents, loadEventResult} from './actions';
import {Actions, UserState} from './types';

export const initialState: UserState = {}

export const userReducer = createReducer<UserState, Actions>(initialState)
                                //Register actions
                                .handleAction(
                                    registerUserAction.request,
                                    (state, action) =>({
                                        ...state,
                                        errorMessage: '',
                                        username: action.payload.username,
                                        email: action.payload.email,
                                    })
                                )
                                .handleAction(
                                    registerUserAction.success,
                                    (state) => ({
                                        ...state,
                                        errorMessage:''
                                    })
                                )
                                .handleAction(
                                    registerUserAction.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                )
                                //Login actions
                                .handleAction(
                                    loginUserAction.request,
                                    (state) => ({
                                        ...state,
                                        errorMessage: ''
                                    })
                                )
                                .handleAction(
                                    loginUserAction.success,
                                    (state, action) => ({
                                        ...state,
                                        auth: {
                                            accessToken: action.payload.accessToken,
                                            owner: false
                                        },
                                        email: action.payload.email,
                                        id: action.payload.id
                                    })
                                )
                                .handleAction(
                                    loginUserAction.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                )
                                .handleAction(
                                    logoutUserAction.success,
                                    (state, action) => ({
                                        ...state,
                                        auth: {
                                            accessToken: "",
                                            owner: false
                                        },
                                    })
                                )
                                .handleAction(
                                    logoutUserAction.request,
                                    (state) => ({
                                        ...state,
                                        errorMessage: ''
                                    })
                                )
                                .handleAction(
                                    loginOwnerAction.request,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: ''
                                    })
                                )
                                .handleAction(
                                    loginOwnerAction.success,
                                    (state, action) => ({
                                        ...state,
                                        auth: {
                                            accessToken: action.payload.accessToken,
                                            owner: true
                                        },
                                    })
                                )
                                .handleAction(
                                    loginOwnerAction.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                )
                                .handleAction(
                                    loadCurrentSong.request,
                                    (state, action) =>({
                                        ...state,
                                        currentsong: action.payload.currentsong,
                                        errorMessage: ''
                                    })
                                )
                                .handleAction(
                                    loadCurrentSong.success,
                                    (state, action) => ({
                                        ...state,
                                        currentsong: action.payload.currentsong,
                                        errorMessage: ''
                                        
                                    })
                                )
                                .handleAction(
                                    loadCurrentSong.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                )
                                .handleAction(
                                    loadUpcommingEvents.request,
                                    (state) => ({
                                        ...state,
                                        errorMessage: ''
                                    })
                                )
                                .handleAction(
                                    loadUpcommingEvents.success,
                                    (state, action) => ({
                                        ...state,
                                        upcommingevents: action.payload.upcommingevents
                                    })
                                )
                                .handleAction(
                                    loadUpcommingEvents.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                )
                                .handleAction(
                                    loadEventResult.request,
                                    (state, action) => ({
                                        ...state,
                                        eventId: action.payload.eventId,
                                        errorMessage: ''
                                    })
                                )
                                .handleAction(
                                    loadEventResult.success,
                                    (state, action) => ({
                                        ...state,
                                        eventResult: action.payload.results
                                    })
                                )
                                .handleAction(
                                    loadEventResult.failure,
                                    (state, action) => ({
                                        ...state,
                                        errorMessage: action.payload.message,
                                    })
                                )