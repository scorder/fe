import {
    all,
    fork, 
    put,
    call, 
    takeLatest,
    select,
} from 'redux-saga/effects';
import ApiCall from '../middleware/api';
import Endpoints from '../../appconfig/apiendpoints';
import {ActionType} from 'typesafe-actions';
import { history } from '../root';
import { event2, result } from './types'
import { loginUserAction, logoutUserAction, registerUserAction, loginOwnerAction, loadCurrentSong, loadUpcommingEvents, loadEventResult } from './actions';
import {addSongs} from '../music/actions';
import {PriceList} from '../drinks/types'
import Routes from '../../appconfig/routes';
import { tokenSelector } from './selectors';
import { Playlist, AllSongsList } from '../music/types';
import {Table} from '../orders/types'
import { loadTables, loadPriceList } from '../orders/actions';
import {ApplicationState} from '../../store/types';
import { EVENT_RESULT_FAILURE } from './constants';

function* handleRegisterUserRequest(action: ActionType<typeof registerUserAction.request>): Generator {
    try {
        const {
            username,
            email,
            password,
            pin,
            location,
        } = action.payload

        const route = Endpoints.register;
        const locationX = location.split("@")[1].split(",")[0];
        const locationY = location.split("@")[1].split(",")[1];
        const name = username;

        (yield call(ApiCall.post, route, {
            data: {
                name,
                email,
                password,
                pin,
                locationX,
                locationY
            },
        }, false)) as {
            accessToken: string
        };

        yield put(registerUserAction.success());
        yield call(history.push, Routes.home);
    } catch (err) {
        yield put(registerUserAction.failure(err));
    }
}

function* handleLoginUserRequest(action: ActionType<typeof loginUserAction.request>): Generator {
    try {

        
        const {
            email,
            password
        } = action.payload;

        const response = (yield call(ApiCall.post, Endpoints.login, {
            data: {
                email: email,
                password: password,    
            }
        }, false)) as {
            accessToken: string,
            email: string,
            id: string
        }

        yield put(loginUserAction.success({
            accessToken: response.accessToken,
            email: response.email,
            id: response.id
        }));

        const response1 = (yield call(ApiCall.get, Endpoints.addsongs, {
        }, false)) as 
             AllSongsList[];
        yield put(addSongs.success({
                allsongs: response1
        }));
        
        yield call(history.push, '/');
        const response2 = (yield call(ApiCall.get, Endpoints.tables.gettable(), {
        }, false)) as Table[];

        yield put(loadTables.success({tables:response2}))


        const response3 = (yield call(ApiCall.get, Endpoints.pricelist, {
            
        }, false)) as PriceList[];
        yield put(loadPriceList.success({pricelist: response3}));

        const response4 = (yield call(ApiCall.get, Endpoints.my, {
        }, false)) as event2[]

        yield put(loadUpcommingEvents.success({
            upcommingevents:response4
        }));

        const response5 = (yield call(ApiCall.get, Endpoints.currentsong, {
        }, false)) as AllSongsList;

        yield put(loadCurrentSong.success({
            currentsong: response5
        }));
        
    } catch (err) {
        yield put(loginUserAction.failure(err));
    }
}

function* handleLoginOwnerRequest(action: ActionType<typeof loginOwnerAction.request>): Generator {
    try {
        
        const {
            pin
        } = action.payload;
        const token = 'Bearer ' + (yield select(tokenSelector));
        const response = (yield call(ApiCall.post, Endpoints.elevate, {
            headers:{
                'Authorization':  token
            },
            data: {
                pin: pin
            }
        }, false)) as {
            accessToken: string,
            email: string
            tokenType: string
        }
        yield put(loginOwnerAction.success({
            accessToken: response.accessToken

        }));
        yield call(history.push, '/');
    } catch (err) {
        yield put(loginOwnerAction.failure(err));
    }
}


function* handleLogoutUserRequest(action: ActionType<typeof logoutUserAction.request>): Generator {
    try{
        yield put(loginOwnerAction.success({
            accessToken: ""
        }));

        yield put(loadCurrentSong.success({
            currentsong: {songTitle:"", artistName:"", songLink:"", albumTitle:"", id:0, isInSongList:false}
        }));

        yield call(history.push, '/');
    }
    
    catch (err) {
        yield put(logoutUserAction.failure(err));
    }
}

function* handleloadCurrentSongRequest(action: ActionType<typeof loadCurrentSong.request>): Generator {
    try{
        
        const response = (yield call(ApiCall.get, Endpoints.currentsong, {
        }, false)) as AllSongsList;

        yield put(loadCurrentSong.success({
            currentsong: response
        }));
        } 
    catch (err) {
        yield put(loadCurrentSong.failure(err));
        }
}

function* handEleventResultRequest(action: ActionType<typeof loadEventResult.request>): Generator {
    try{
        const {
            eventId
        } = action.payload;
        const response1 = (yield call(ApiCall.get, Endpoints.eventsResult.getall(eventId), {
        }, false)) as result[];

        console.log(response1)
        const res:result[] = response1

        yield put(loadEventResult.success({
            results: res
        }));

        yield call(history.push, Routes.home);
        } 
    catch (err) {
        yield put(loadEventResult.failure(err));
        }
}

function* watchRegisterUserRequestSaga(): Generator {
    yield takeLatest(registerUserAction.request, handleRegisterUserRequest);
}

function* watchLoginUserRequestSaga(): Generator {
    yield takeLatest(loginUserAction.request, handleLoginUserRequest)
}

function* watchLogoutUserSaga(): Generator{
    yield takeLatest(logoutUserAction.request, handleLogoutUserRequest);
}
function* watchLoginOwnerRequestSaga(): Generator {
    yield takeLatest(loginOwnerAction.request, handleLoginOwnerRequest)
}

function* watchCurrentSongRequestSaga(): Generator {
    yield takeLatest(loadCurrentSong.request, handleloadCurrentSongRequest)
}

function* watchEventResultRequestSaga(): Generator {
    yield takeLatest(loadEventResult.request, handEleventResultRequest)
}

function* userSaga(): Generator {
    yield all([
        fork(watchRegisterUserRequestSaga),
        fork(watchLoginUserRequestSaga),
        fork(watchLogoutUserSaga),
        fork(watchLoginOwnerRequestSaga),
        fork(watchCurrentSongRequestSaga),
        fork(watchEventResultRequestSaga),
    ]);
}

export default userSaga;