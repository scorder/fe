import { 
    registerUserAction,
    loginUserAction, 
    logoutUserAction,
    loginOwnerAction,
    loadCurrentSong,
    loadUpcommingEvents,
    loadEventResult,
} from './actions';
import {Playlist, AllSongsList} from '../music/types'
import { ActionType } from 'typesafe-actions';

export interface RegisterUserRequestPayload {
    username: string;
    email: string;
    password: string;
    location: string;
    pin: string;
}

export interface LoginUserRequestPayload {
    email: string;
    password: string;
}

export interface LoginOwnerRequestPayload {
    pin: string;
}

export interface LoginOwnerSuccessPayload{
    accessToken: string;
}

export interface LoginUserSuccessPayload {
    accessToken: string;
    email: string;
    id: string;
}

export interface LogoutUserSuccessPayload{
    accessToken: string;
}

export interface LogoutUserRequestPayload{
}

export interface CurrentSongRequestPayload {
    currentsong: AllSongsList;
}

export interface CurrentSongSuccessPayload{
    currentsong: AllSongsList;
}

export interface result{
    place: number;
    username: string; 
    percentage: number;
}

export interface EventResultSuccessPayload {
    results: result[];
}

export interface event2{
    id:number,
    eventName:string,
    date:string,
    ending:string,
    eventInfo:string
}

export interface UpcommingEventsPayload{
    upcommingevents: event2[];
}

export interface EventResultRequestPayload{
    eventId: number;
}

export interface UserState {
    errorMessage?: string;
    auth?: {
        accessToken: string;
        owner: boolean;
    }
    email?: string;
    id?: string;
    currentsong?: AllSongsList;
    upcommingevents?: event2[];
    eventResult?: result[];
    eventId?:  number;
}

export type Actions = ActionType<typeof registerUserAction 
                                | typeof loginUserAction 
                                | typeof logoutUserAction
                                | typeof loginOwnerAction
                                | typeof loadCurrentSong
                                | typeof loadUpcommingEvents 
                                | typeof loadEventResult>;